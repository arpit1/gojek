package com.arpit.gojek

import android.app.Application
import android.content.Context
import com.arpit.gojek.data.preferences.PreferenceProvider
import com.arpit.gojek.network.NetworkConnectionInterceptor
import org.kodein.di.Kodein
import org.kodein.di.KodeinAware
import org.kodein.di.android.x.androidXModule
import org.kodein.di.generic.bind
import org.kodein.di.generic.instance
import org.kodein.di.generic.singleton

class MyApplication: Application(), KodeinAware {

    companion object {
        private lateinit var application: MyApplication

        @JvmStatic
        fun getInstance(): MyApplication {
            return application
        }
    }

    override fun onCreate() {
        super.onCreate()
        application = this
    }

    operator fun get(context: Context): MyApplication {
        return context.applicationContext as MyApplication
    }

    override val kodein = Kodein.lazy {
        import(androidXModule(this@MyApplication))

//        bind() from singleton { NetworkModule(instance()) }

        bind() from singleton { NetworkConnectionInterceptor(instance()) }

        bind() from singleton { PreferenceProvider(instance()) }

//        bind() from provider { GenerateImageViewModelFactory(instance()) }
    }
}