package com.arpit.gojek.common

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.arpit.gojek.R
import com.arpit.gojek.notifiers.Loader
import com.arpit.gojek.notifiers.Notify
import com.arpit.gojek.notifiers.NotifyException
import com.arpit.gojek.notifiers.NotifyRetry
import com.arpit.gojek.util.Utility
import com.arpit.gojek.util.showErrorSnackBar
import com.arpit.gojek.util.showSnackBarWithRetry
import com.splunk.mint.Mint
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.kodein.di.KodeinAware
import org.kodein.di.android.kodein

abstract class BaseActivity: AppCompatActivity(), KodeinAware {

    override val kodein by kodein()
    private lateinit var baseBinding: ViewDataBinding

    @ExperimentalCoroutinesApi
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        /**
         * Add your apiKey to track the crashes of the app.
         * Create your account on Splunk Mint and add the app over there to get the api Key.
         */
        Mint.initAndStartSession(this.application, "f1f1f1f1f1")

        if (!dataBinding) {
            setContentView(layoutResource)
        } else {
            baseBinding = DataBindingUtil.setContentView(this, layoutResource)
        }
        initializeViewModel()
        setBindings()
        getViewModel()?.let {
            it.notifier.recieve { event ->
                when (event) {
                    is NotifyException -> {
                        event.exception.message?.let { msg ->
                            applicationContext.showErrorSnackBar(getBinding().root, msg)
                        }
                    }
                    is Loader -> {
                        if (event.loading) {
                            Utility.showProgressDialog(this)
                        } else {
                            Utility.hideProgressDialog()
                        }
                    }
                    is NotifyRetry -> {
                        getBinding().root.showSnackBarWithRetry(getString(R.string.text_check_internet), event.call)
                    }
                    else -> {
                        onNotificationReceived(event)
                    }
                }
            }
        }
    }

    fun getBinding(): ViewDataBinding {
        return baseBinding
    }

    abstract val dataBinding: Boolean
    abstract val layoutResource: Int
    abstract fun getViewModel(): BaseViewModel?
    abstract fun onNotificationReceived(data: Notify)
    abstract fun setBindings()
    abstract fun initializeViewModel()

    inline fun <reified T> lazyBinding(): Lazy<T> = lazy { getBinding() as T }
}