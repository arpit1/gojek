package com.arpit.gojek.data.contract

interface ErrorActionListener {
    fun onErrorActionClicked()
}