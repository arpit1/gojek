package com.arpit.gojek.data.model

data class ImageUploadResponse(
    var id: String? = null,
    var url: String? = null
)