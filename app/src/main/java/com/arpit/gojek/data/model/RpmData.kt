package com.arpit.gojek.data.model

data class RpmData(
    var status: String? = null,
    var min: Int? = 0,
    var max: Int? = 0,
    var random: Int = 0
)