package com.arpit.gojek.network

import com.arpit.gojek.data.model.RpmData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import com.arpit.gojek.data.model.Result
import com.arpit.gojek.repository.HomeDataRepository

val ioScope: CoroutineScope = CoroutineScope(Dispatchers.IO)
val mainScope: CoroutineScope = CoroutineScope(Dispatchers.Main)

fun fetchRpmDetails(
    block: (response: Result<List<RpmData>>?) -> Unit
) {
    ioScope.launch {
        val response = HomeDataRepository(
            NetworkModule.formService
        ).getRpmDetails()
        block(response)
    }
}