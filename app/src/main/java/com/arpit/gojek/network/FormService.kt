package com.arpit.gojek.network

import com.arpit.gojek.data.model.RpmData
import com.arpit.gojek.data.model.ServerResponse
import retrofit2.Response
import retrofit2.http.*

interface FormService {
    @GET("csrng.php")
    suspend fun getRpmCount(@Query("min") min: Int, @Query("max") max: Int): Response<List<RpmData>>
}