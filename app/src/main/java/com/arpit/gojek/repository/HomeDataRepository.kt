package com.arpit.gojek.repository

import com.arpit.gojek.common.BaseRepository
import com.arpit.gojek.data.model.RpmData
import com.arpit.gojek.network.FormService
import com.arpit.gojek.data.model.Result
import com.arpit.gojek.data.model.ServerResponse

class HomeDataRepository(private val formService: FormService): BaseRepository() {

    suspend fun getRpmDetails(): Result<List<RpmData>>? {
        val response = safeApiCall(
            call = {
                formService.getRpmCount(min = 0, max = 100)
            },
            errorMessage = "Failed to fetch Details"
        )

        return response?.let {
            Result.Success(it)
        }
    }
}