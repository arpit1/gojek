package com.arpit.gojek.ui.home

import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.view.animation.LinearInterpolator
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProvider
import com.arpit.gojek.R
import com.arpit.gojek.common.BaseActivity
import com.arpit.gojek.common.BaseViewModel
import com.arpit.gojek.databinding.ActivityMainBinding
import com.arpit.gojek.notifiers.Notify
import com.arpit.gojek.ui.home.HomeViewModel.Companion.SPEED_MODIFIED
import com.arpit.gojek.ui.home.HomeViewModel.Companion.STOP_ANIMATION
import kotlinx.android.synthetic.main.activity_main.*


class HomeActivity : BaseActivity() {

    private lateinit var viewModel: HomeViewModel
    private lateinit var toolbar: Toolbar
    private val binding: ActivityMainBinding by lazyBinding()

    override val dataBinding: Boolean = true
    override val layoutResource: Int = R.layout.activity_main
    override fun getViewModel(): BaseViewModel {
        return viewModel
    }

    override fun initializeViewModel() {
        viewModel = ViewModelProvider(this).get(HomeViewModel::class.java)
    }

    override fun setBindings() {
        binding.viewModel = viewModel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setToolbar()
    }

    private fun setToolbar() {
        toolbar = findViewById(R.id.toolbar)

        toolbar.title = resources.getString(R.string.title)
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white))
        toolbar.setBackgroundColor(ContextCompat.getColor(this, R.color.colorPrimary))
    }

    private fun setAnimation(animDuration: Long) {
        rpm_count.text = resources.getString(R.string.rpm_count, 60000 / animDuration)
        val rotateLoading: Animation = AnimationUtils.loadAnimation(this, R.anim.rotate)
        rotateLoading.duration = animDuration
        rotateLoading.interpolator = LinearInterpolator()
        rotateLoading.repeatCount = Animation.INFINITE
        spin_wheel.clearAnimation()
        spin_wheel.animation = rotateLoading
    }

    private fun stopAnimation() {
        rpm_count.text = resources.getString(R.string.rpm_count, 0)
        spin_wheel.clearAnimation()
    }

    override fun onNotificationReceived(data: Notify) {
        when (data.identifier) {
            SPEED_MODIFIED -> {
                setAnimation((data.arguments[0] as Int).toLong())
            }
            STOP_ANIMATION -> {
                stopAnimation()
            }
        }
    }
}