package com.arpit.gojek.ui.home

import androidx.databinding.ObservableBoolean
import com.arpit.gojek.common.BaseViewModel
import com.arpit.gojek.data.model.Result
import com.arpit.gojek.network.fetchRpmDetails
import com.arpit.gojek.notifiers.Notify
import com.arpit.gojek.notifiers.NotifyException
import com.arpit.gojek.notifiers.NotifyRetry
import com.arpit.gojek.util.Constant.Companion.FETCHING_RPM
import com.arpit.gojek.util.Constant.Companion.SPEED_UPDATED
import com.arpit.gojek.util.Constant.Companion.UNABLE_TO_UPDATE_SPEED
import com.arpit.gojek.util.SomethingWentWrongException
import com.arpit.gojek.util.isInternetAvailable

class HomeViewModel : BaseViewModel() {

    var enableSpeedButton = ObservableBoolean(true)

    fun getRpmDetails() {
        if (isInternetAvailable()) {
            showProgress()
            enableSpeedButton.set(false)
            notifier.notify(NotifyException(SomethingWentWrongException(FETCHING_RPM)))
            fetchRpmDetails { result ->
                when (result) {
                    is Result.Success -> {
                        hideProgress()
                        if (result.data.isNotEmpty()) {
                            val randomValue = result.data[0].random
                            if (randomValue > 0) {
                                val duration = 60000/randomValue
                                notifier.notify(Notify(SPEED_MODIFIED, duration))
                            } else {
                                notifier.notify(Notify(STOP_ANIMATION, randomValue))
                            }
                            notifier.notify(
                                NotifyException(
                                    SomethingWentWrongException(
                                        SPEED_UPDATED
                                    )
                                )
                            )
                        }
                        enableSpeedButton.set(true)
                    }
                    is Result.Error -> {
                        hideProgress()
                        enableSpeedButton.set(true)
                        notifier.notify(
                            NotifyException(
                                SomethingWentWrongException(
                                    UNABLE_TO_UPDATE_SPEED
                                )
                            )
                        )
                        result.exception.printStackTrace()
                    }
                    else -> {
                        enableSpeedButton.set(true)
                        notifier.notify(
                            NotifyException(
                                SomethingWentWrongException(
                                    UNABLE_TO_UPDATE_SPEED
                                )
                            )
                        )
                        hideProgress()
                    }
                }
            }
        } else {
            notifier.notify(NotifyRetry { getRpmDetails() })
        }
    }

    companion object {
        const val SPEED_MODIFIED = "SPEED_MODIFIED"
        const val STOP_ANIMATION = "STOP_ANIMATION"
    }
}