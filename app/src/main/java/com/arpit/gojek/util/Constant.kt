package com.arpit.gojek.util

class Constant {
    companion object {
        const val ON_ERROR = "ON_ERROR"
        const val NO_INTERNET = "No Internet"
        const val RETRY = "RETRY"
        const val FETCHING_RPM = "FETCHING RPM"
        const val UNABLE_TO_UPDATE_SPEED = "UNABLE TO UPDATE SPEED"
        const val SPEED_UPDATED = "SPEED UPDATED"
        const val NO_INTERNET_CONNECTION_ERROR = "Uh, oh! It seems like you are not connected to the internet. Please connect to a network and try again."
    }
}