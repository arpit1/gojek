package com.arpit.gojek.util

import java.io.IOException

class NoInternetException(message: String) : IOException(message)

class ApiException(message: String) : IOException(message)

class SomethingWentWrongException(message: String) : RuntimeException(message)