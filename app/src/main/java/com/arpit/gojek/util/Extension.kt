package com.arpit.gojek.util

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.view.Gravity
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import com.arpit.gojek.MyApplication
import com.arpit.gojek.R
import com.arpit.gojek.data.contract.ErrorActionListener
import com.arpit.gojek.data.model.ErrorModel
import com.google.android.material.snackbar.Snackbar
import java.util.regex.Pattern

fun Context.showToast(message: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, duration).show()
}

fun Context.showErrorSnackBar(view: View?, msg: String) {
    try {
        if (view != null) {
            val snackbar = Snackbar
                .make(view, msg, Snackbar.LENGTH_SHORT)
                .setAction("RETRY", null)
                .setBackgroundTint(ContextCompat.getColor(this, R.color.black))

            // Changing action button text color
            val sbView = snackbar.view
            val textView = sbView.findViewById<TextView>(R.id.snackbar_text)
            textView.gravity = Gravity.CENTER_HORIZONTAL

            textView.setTextColor(ContextCompat.getColor(this, R.color.white))
            snackbar.show()
        }
    } catch (e: Exception) {
        e.logOnCrashAnalytics()
    }
}

fun View.showSnackBarWithRetry(msg: String, callBack: () -> Unit) {
    try {
        val snackbar = Snackbar.make(this, msg, Snackbar.LENGTH_INDEFINITE)
            .setAction("RETRY") {
                callBack.invoke()
            }
            .setActionTextColor(ContextCompat.getColor(this.context, R.color.colorPrimary))
            .setBackgroundTint(ContextCompat.getColor(this.context, R.color.black))
            .setTextColor(ContextCompat.getColor(this.context, R.color.white))
            .setAnimationMode(Snackbar.ANIMATION_MODE_SLIDE)
        snackbar.show()
    } catch (e: Exception) {
        e.logOnCrashAnalytics()
    }
}

fun Exception.logOnCrashAnalytics() {
    this.printStackTrace()
}

fun isInternetAvailable(): Boolean {
    var result = false
    val connectivityManager = MyApplication.getInstance()
        .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?

    connectivityManager?.let {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            it.getNetworkCapabilities(connectivityManager.activeNetwork)?.apply {
                result = when {
                    hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                    hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                    else -> false
                }
            }
        } else {
            connectivityManager.activeNetworkInfo.also { networkInfo ->
                return networkInfo != null && networkInfo.isConnected
            }
        }
    }
    return result
}

fun ErrorModel.showErrorModel(
    title: String,
    subTitle: String,
    button: String,
    action: () -> Unit
) {
    this.apply {
        errorTitle = title
        errorSubTitle = subTitle
        buttonText = button
        errorActionListener = object : ErrorActionListener {
            override fun onErrorActionClicked() {
                action()
            }
        }
    }
}

fun String.isValidMobileNumber(): Boolean{
    val mobileValidationPattern = Pattern.compile("^[6-9]\\d{9}$")
    return !(this.isEmpty() || this.length < 10 || !mobileValidationPattern.matcher(this).matches())
}